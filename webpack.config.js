const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin')

const isDevelopment = process.env.NODE_ENV === 'development'

module.exports = {
    entry: ["./src/index.js"],
    output: {
        path: path.join(__dirname, "/dist"),
        filename: "bundle.js"
    },

    devServer: {
        hot: true,
        inline: true,
        host: '192.168.0.7',
        port: '3000',
        watchOptions: {
            ignored: /node_modules/,
            aggregateTimeout: 100,
            poll: 300
        }
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.module\.(s(a|c)ss)$/,
                loader: [
                    // isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: isDevelopment
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDevelopment
                        }
                    }
                ]
            },
            {
                test: /\.s(a|c)ss$/,
                exclude: /\.module.(s(a|c)ss)$/,
                loader: [
                    // isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: isDevelopment
                        }
                    }
                ]
            },
            {
                test: /\.svg$/,
                loader: 'svg-url-loader'
            }
        ]
    },
    resolve: {
        extensions: [
            '.js',
            '.jsx',
            '.sass'
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
        // new ExtractTextPlugin('[name].css', { allChunks: true, disable: isDevelopment }),
        // new MiniCssExtractPlugin({
        //     filename: isDevelopment ? '[name].css?[hash]' : '[name].[hash].css',
        //     chunkFilename: isDevelopment ? '[id].css?[hash]' : '[id].[hash].css'
        // })
    ]
};