import React, { Component } from "react";
import s from './Input.module.sass'

export class Input extends Component {

  // ---------------------city---------------------
  handleOnChangeSelect = (e) => {
    this.props.change(e.currentTarget.value)
  }
  // ----------------------------------------------

  // ---------------------password---------------------
  handleOnChangePassword = (e) => {
    this.props.change(e.currentTarget.value)
  }

  renderMistPassword() {
    if (this.props.mist === 'empty') {
      return (
        <React.Fragment>
          <input
            onChange={this.handleOnChangePassword}
            className={`${s.inputBlock__input} ${s.mistakeInput}`}
            defaultValue={this.props.value}
            type={this.props.data.password} />
          <p className={s.mistakeMessage}>Укажите пароль</p>
        </React.Fragment>
      )
    } else if (this.props.mist === 'wrong') {
      return (
        <React.Fragment>
          <input
            onChange={this.handleOnChangePassword}
            className={`${s.inputBlock__input} ${s.mistakeInput}`}
            defaultValue={this.props.value}
            type={this.props.data.password} />
          <p className={s.mistakeMessage}>Используйте не менее 5 символов</p>
        </React.Fragment>
      )
    } else {
      return (
        <input
          onChange={this.handleOnChangePassword}
          className={s.inputBlock__input}
          defaultValue={this.props.value}
          type={this.props.data.password} />
      )
    }
  }
  // --------------------------------------------------

  // ---------------------passwordRep---------------------
  handleOnChangePasswordRep = (e) => {
    this.props.change(e.currentTarget.value)
  }

  renderMistPasswordRepeat() {
    if (this.props.mist === 'empty') {
      return (
        <React.Fragment>
          <input
            onChange={this.handleOnChangePasswordRep}
            className={`${s.inputBlock__input} ${s.mistakeInput}`}
            defaultValue={this.props.value}
            type={this.props.data.passwordRepeat} />
          <p className={s.mistakeMessage}>Укажите пароль</p>
        </React.Fragment>
      )
    } else if (this.props.mist === 'wrong') {
      return (
        <React.Fragment>
          <input
            onChange={this.handleOnChangePassword}
            className={`${s.inputBlock__input} ${s.mistakeInput}`}
            defaultValue={this.props.value}
            type={this.props.data.passwordRepeat} />
          <p className={s.mistakeMessage}>Пароли не совпадают</p>
        </React.Fragment>
      )
    } else {
      return (
        <input
          onChange={this.handleOnChangePasswordRep}
          className={s.inputBlock__input}
          defaultValue={this.props.value}
          type={this.props.data.passwordRepeat} />
      )
    }
  }
  // --------------------------------------------------

  // ---------------------email---------------------
  handleOnChangeEmail = (e) => {
    this.props.change(e.currentTarget.value)
  }

  renderMistEmail() {
    if (this.props.mist === 'empty') {
      return (
        <React.Fragment>
          <input
            onChange={this.handleOnChangeEmail}
            className={`${s.inputBlock__input} ${s.mistakeInput}`} />
          <p className={s.mistakeMessage}>Укажите E-mail</p>
        </React.Fragment>
      )
    } else if (this.props.mist === 'wrong') {
      return (
        <React.Fragment>
          <input
            onChange={this.handleOnChangeEmail}
            className={`${s.inputBlock__input} ${s.mistakeInput}`}
            defaultValue={this.props.value}/>
          <p className={s.mistakeMessage}>Неверный E-mail</p>
        </React.Fragment>
      )
    } else {
      return (
        <input
          onChange={this.handleOnChangeEmail}
          className={s.inputBlock__input}
          defaultValue={this.props.value}/>
      )
    }
  }
  // --------------------------------------------------

  // ---------------------checkbox---------------------
  handleOnClickCheckbox = (e) => {
    this.props.change(e.currentTarget.checked)
  }
  // --------------------------------------------------

  // ---------------------lastChange---------------------
  getChangeDate = () => {
    const changeDate = new Date().toLocaleString('ru', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    })
    const changeTime = new Date().toLocaleString('ru', {
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric'
    })
    const changeDateTime = `${changeDate.slice(0, changeDate.length - 3)} в ${changeTime}`
    this.props.change(changeDateTime)
  }
  // --------------------------------------------------

  renderInput() {
    //select block
    if (this.props.data.select) {
      let citiesDropDown = null

      citiesDropDown = this.props.data.inputValue.map((currentCity, i) => {
        return (
          <option key={i} value={currentCity}>
            {currentCity}
          </option>
        )
      })

      return (
        <div className={`${s.inputBlock}`}>
          <div className={`${s.inputBlock__text}`}>{this.props.data.text} </div>
          <div className={`${s.selectWrapper}`}>
            <select
              onChange={this.handleOnChangeSelect}
              className={`${s.inputBlock__input} ${s.inputBlock__dropdown}`}>
              {citiesDropDown}
            </select>
          </div>
        </div>
      )
    }

    //password block
    if (this.props.data.password) {
      return (
        <div className={`${s.inputBlock}`}>
          <div className={`${s.inputBlock__text}`}>{this.props.data.text} </div>
          {this.renderMistPassword()}
          <p className={s.inputBlock__description}>{this.props.data.description}</p>
        </div>
      )
    }

    //password repeat block
    if (this.props.data.passwordRepeat) {
      return (
        <div className={`${s.inputBlock} ${s.pb25}`}>
          <div className={`${s.inputBlock__text}`}>{this.props.data.text} </div>
          {this.renderMistPasswordRepeat()}
          <p className={s.inputBlock__description}>{this.props.data.description}</p>
        </div>
      )
    }

    //email block
    if (this.props.data.mail) {
      return (
        <div className={`${s.inputBlock}`}>
          <div className={`${s.inputBlock__text}`}>{this.props.data.text} </div>
          {this.renderMistEmail()}
          <p className={s.inputBlock__description}>{this.props.data.description}</p>
        </div>
      )
    }

    //agree block
    if (this.props.data.checkbox) {
      return (
        <div className={`${s.inputBlock}`}>
          <div className={`${s.inputBlock__text}`}>{this.props.data.text} </div>
          <input
            onClick={this.handleOnClickCheckbox}
            className={s.inputBlock__checkbox}
            type={this.props.data.checkbox} />
          <p className={s.inputBlock__descriptionMail}>{this.props.data.inputText}</p>
        </div>
      )
    }

    //change block
    if (this.props.data.btn) {
      return (
        <div className={`${s.inputBlock}`}>
          <div className={`${s.inputBlock__text}`}></div>
          <button
            onClick={this.getChangeDate}
            className={s.inputBlock__btn}>{this.props.data.textBtn}
          </button>
          <p className={s.inputBlock__description}>{this.props.data.description} {this.props.lastChange}</p>
        </div>
      )
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.renderInput()}
      </React.Fragment>
    )
  }
}