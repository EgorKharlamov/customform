import React, { Component } from "react";
import s from './Header.module.sass';

export class Header extends Component {

  state = {
    isVisibleChangeStatus: false
  }

  handleOnClickStatus = (e) => {
    this.setState({ isVisibleChangeStatus: !this.state.isVisibleChangeStatus })
  }

  handleOnChangeStatus = (e) => {
    const { value } = e.currentTarget
    this.setState({ tooltip: value })
  }

  handleOnClickSaveStatus = (e) => {
    this.state.tooltip ? this.props.onChangeTooltip(this.state.tooltip) : this.props.onChangeTooltip(this.props.tooltip)
    this.handleOnClickStatus()
  }

  renderChangeStatus = () => {
    return <label className={`${s.header__statusChange} ${s.statusChange}`}>
      <input
        className={s.statusChange__input}
        onChange={this.handleOnChangeStatus}
        defaultValue={this.props.tooltip}>
      </input>

      <button
        className={s.statusChange__btn}
        onClick={this.handleOnClickSaveStatus}>
        Сохранить
      </button>
    </label>
  }

  render() {
    const { id, hello, name, status, tooltip } = this.props
    return (
      <div className={s.header}>

        <h1 className={s.header__title}>
          <span className={`${s.gray}`}>{hello},</span> {`${name}${id}`}
        </h1>

        <a href='#'
          onClick={this.handleOnClickStatus}
          className={s.header__status}>{status}
        </a>

        {this.state.isVisibleChangeStatus && this.renderChangeStatus()}

        <div className={s.header__tooltip}>{tooltip}</div>

      </div>
    );
  }
}