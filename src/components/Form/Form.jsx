import React, { Component } from "react";
import { HeaderContainer } from '../../containers/HeaderContainer.jsx';

import s from './Form.module.sass';
import { Input } from "./Input/Input.jsx";

class Form extends Component {

  state = {
    inputCities: {
      text: 'Ваш город',
      select: 'select',
      inputValue: this.getCitiesNames(),
      maxPopCity: this.getCitiesNames()[0]
    },
    inputPassword: {
      text: 'Пароль',
      password: 'password',
      description: 'Ваш новый пароль должен содержать не менее 5 символов.'
    },
    inputPasswordRepeat: {
      text: 'Пароль еще раз',
      passwordRepeat: 'password',
      description: 'Повторите пароль, пожалуйста, это обезопасит вас с нами на случай ошибки.'
    },
    inputEmail: {
      text: 'Электронная почта',
      mail: 'mail',
      description: 'Можно изменить адрес, указанный при регистрации.'
    },
    inputAgree: {
      text: 'Я согласен',
      checkbox: 'checkbox',
      inputText: 'принимать актуальную информацию на емейл'
    },
    inputChange: {
      textBtn: 'Изменить',
      btn: 'btn',
      description: 'последние изменения'
    },
    toJsonCity: this.getCitiesNames()[0],
    //
    password: '',
    passwordValid: false,
    passwordRep: '',
    passwordRepValid: false,
    //
    emailValid: false,
    toJsonEmail: '',
    toJsonAgree: false,
    toJsonLastChange: '---',
  }

  getCitiesNames() {
    let citiesNames = []
    if (this.props.cities.length) {
      this.props.cities.map((currentCity) => {
        citiesNames.push(currentCity.city)
      })
    }
    return citiesNames
  }

  // ---------------------city---------------------
  handleOnChangeSelect = (data) => {
    this.setState({ toJsonCity: data })
  }
  // ----------------------------------------------

  // ---------------------password---------------------
  handleOnChangePassword = (data) => {
    if (data.trim() && data.length >= 5) {
      this.setState({ passwordValid: true })
    } else {
      this.setState({ passwordValid: false })
    }
    this.setState({ password: data })
  }
  // ----------------------------------------------

  // ---------------------passwordRepeat---------------------
  handleOnChangePasswordRep = (data) => {
    if (this.state.password === data && data.length) {
      this.setState({ passwordRepValid: true })
    } else {
      this.setState({ passwordRepValid: false })
    }
    this.setState({ passwordRep: data })
  }
  // ----------------------------------------------

  // ---------------------email---------------------
  handleOnChangeEmail = (data) => {
    const dataArr = data.split('')
    for (let i = 0; i <= data.length; i++) {
      if (i != 0 && i < data.length - 2 && dataArr[i] === '@') {
        this.setState({ emailValid: true })
        break
      } else {
        this.setState({ emailValid: false })
      }
    }
    this.setState({ toJsonEmail: data })
  }
  // ----------------------------------------------

  // ---------------------agree---------------------
  handleOnChangeAgree = (data) => {
    this.setState({ toJsonAgree: data })
  }
  // ----------------------------------------------

  // ---------------------lastChange---------------------
  getChangeDate = (data) => {
    const { toJsonCity, password, passwordRep, toJsonEmail, toJsonAgree, emailValid, passwordRepValid, passwordValid } = this.state
    if (emailValid && passwordRepValid && passwordValid) {
      const jsonObj = {
        city: toJsonCity,
        password: password,
        email: toJsonEmail,
        agree: toJsonAgree
      }
      console.log(JSON.stringify(jsonObj))
      this.setState({ toJsonLastChange: data })
    }
    // validator password
    if (!passwordValid && !password.trim().length) {
      this.setState({ mistPassword: 'empty' })
    } else if (!passwordValid && password.trim().length) {
      this.setState({ mistPassword: 'wrong' })
    } else {
      this.setState({ mistPassword: 'you are awesome' })
    }

    // validator password repeat
    if (!passwordRepValid && !passwordRep.trim().length) {
      this.setState({ mistPasswordRep: 'empty' })
    } else if (passwordRep != password && passwordRep.trim().length) {
      this.setState({ mistPasswordRep: 'wrong' })
    } else {
      this.setState({ mistPasswordRep: 'you are awesome' })
    }

    // validator email
    if (!emailValid && !toJsonEmail.trim().length) {
      this.setState({ mistEmail: 'empty' })
    } else if (!emailValid && toJsonEmail.trim().length) {
      this.setState({ mistEmail: 'wrong' })
    } else {
      this.setState({ mistEmail: 'you are awesome' })
    }
  }
  // ----------------------------------------------

  render() {
    return (
      <div className={s.form}>
        <div className={s.form__item}>
          <HeaderContainer />
        </div>
        <div className={s.form__item}>
          <Input data={this.state.inputCities} change={this.handleOnChangeSelect} />
        </div>
        <hr />
        <div className={s.form__item}>
          <Input
            data={this.state.inputPassword}
            change={this.handleOnChangePassword} 
            mist={this.state.mistPassword}
            value={this.state.password}/>
        </div>
        <div className={s.form__item}>
          <Input 
          data={this.state.inputPasswordRepeat} 
          change={this.handleOnChangePasswordRep} 
          mist={this.state.mistPasswordRep}
          value={this.state.passwordRep}/>
        </div>
        <hr />
        <div className={s.form__item}>
          <Input 
          data={this.state.inputEmail} 
          change={this.handleOnChangeEmail} 
          mist={this.state.mistEmail}
          value={this.state.toJsonEmail}/>
        </div>
        <div className={s.form__item}>
          <Input data={this.state.inputAgree} change={this.handleOnChangeAgree} />
        </div>
        <div className={s.form__item}>
          <Input
            data={this.state.inputChange}
            change={this.getChangeDate}
            lastChange={this.state.toJsonLastChange} />
        </div>
      </div>
    );
  }
}

export default Form;