import React, { Component } from "react";
import { Header } from '../components/Form/Header/Header.jsx'

export class HeaderContainer extends Component {

  state = {
    id: 3596941,
    hello: 'Здравствуйте',
    name: 'Человек №',
    status: 'Сменить статус',
    tooltip: 'Прежде чем действовать, надо понять',
  }

  handleOnChangeTooltip = (data) => {
    this.setState({ tooltip: data })
  }

  render() {
    const { id, hello, name, status, tooltip } = this.state
    return (<Header
      id={id}
      hello={hello}
      name={name}
      status={status}
      tooltip={tooltip}
      onChangeTooltip={this.handleOnChangeTooltip} />
    )
  }
}