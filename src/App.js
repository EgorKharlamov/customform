import React, { Component } from "react";
import Form from './components/Form/Form.jsx'
import citiesObj from './data/cities.json'

import './App.sass';

class App extends Component {

    getCities() {
        let sortedCities = []
        let maxPopulationCity = { city: '', population: 0 }
        citiesObj.map((currentCity) => maxPopulationCity.population < +currentCity.population ? maxPopulationCity = currentCity : null)
        sortedCities.push(maxPopulationCity)
        citiesObj.map((currentCity) => currentCity.population > 50000 && currentCity.city != maxPopulationCity.city ? sortedCities.push(currentCity) : null)
        return sortedCities
    }

    render() {
        return (
            <div className='wrapper'>
                <Form cities={this.getCities()} />
            </div>
        );
    }
}

export default App;